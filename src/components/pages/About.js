import React from 'react';

function About() {
  return (
    <React.Fragment>
      <div style={pageStyle}>
        <h1>About</h1>
        <p>A todo list built with React</p>
      </div>
    </React.Fragment>
  )
}

const pageStyle = {
  textAlign: 'center',
  marginTop: '10px'
};

export default About;