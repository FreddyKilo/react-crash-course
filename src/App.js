import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import About from "./components/pages/About";
import Header from './components/layout/Header';
import Todos from "./components/Todos";
import AddTodo from "./components/AddTodo";
import axios from 'axios';
import './App.css';

class App extends React.Component {
  state = {
    todos: []
  };

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/todos?_limit=10')
      .then(resp => this.setState({todos: resp.data}));
  }

  toggleCompleted = (id) => {
    this.setState({
      todos: this.state.todos.map(todo => {
        if (todo.id === id) {
          todo.completed = !todo.completed;
        }
        return todo;
      })
    })
  };

  addTodo = (title) => {
    if (!title.trim().length) return;
    axios.post('https://jsonplaceholder.typicode.com/todos', {title: title})
      .then(resp => this.setState({todos: [...this.state.todos, resp.data]}));
  };

  deleteTodo = (id) => {
    axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
      .then(resp => this.setState({
        todos: this.state.todos.filter(todo => todo.id
          !== id)
      }))
  };

  render() {
    return (
      <Router>
        <div className="App">
          <div>
            <Header/>
            <Route exact path='/' render={props => (
              <React.Fragment>
                <AddTodo addTodo={this.addTodo}/>
                <Todos todos={this.state.todos} toggleCompleted={this.toggleCompleted}
                       deleteTodo={this.deleteTodo}/>
              </React.Fragment>
            )}/>
            <Route path='/about' component={About}/>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
